// Copyright 2019 Jackson Harmer
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <Arduino.h>

constexpr auto BUZZER_PIN = A5;

constexpr auto DEFAULT_WORK_HOURS = 0;
constexpr auto DEFAULT_WORK_MINUTES = 25;
constexpr auto DEFAULT_WORK_SECONDS = 0;
constexpr auto DEFAULT_WORK_NUM_BEEPS = 3;
constexpr auto DEFAULT_WORK_BEEP_DUR = 300;
constexpr auto DEFAULT_WORK_BEEP_GAP = 100;

constexpr auto DEFAULT_SHORT_BREAK_HOURS = 0;
constexpr auto DEFAULT_SHORT_BREAK_MINUTES = 5;
constexpr auto DEFAULT_SHORT_BREAK_SECONDS = 0;
constexpr auto DEFAULT_SHORT_BREAK_NUM_BEEPS = 1;
constexpr auto DEFAULT_SHORT_BREAK_BEEP_DUR = 300;
constexpr auto DEFAULT_SHORT_BREAK_BEEP_GAP = 0;

constexpr auto DEFAULT_LONG_BREAK_HOURS = 0;
constexpr auto DEFAULT_LONG_BREAK_MINUTES = 15;
constexpr auto DEFAULT_LONG_BREAK_SECONDS = 0;
constexpr auto DEFAULT_LONG_BREAK_NUM_BEEPS = 2;
constexpr auto DEFAULT_LONG_BREAK_BEEP_DUR = 300;
constexpr auto DEFAULT_LONG_BREAK_BEEP_GAP = 100;

constexpr auto DEFAULT_WORK_TO_LONG_BREAK = 4;

constexpr auto DEFAULT_I2C_PIN = 0x27;
constexpr auto DEFAULT_LCD_ROWS = 4;
constexpr auto DEFAULT_LCD_COLS = 20;
