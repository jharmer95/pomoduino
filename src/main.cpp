// Copyright 2019 Jackson Harmer
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "config.hpp"

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(DEFAULT_I2C_PIN, DEFAULT_LCD_COLS, DEFAULT_LCD_ROWS);

void beep(uint8_t numbeeps, unsigned long duration, unsigned long gap = 100U)
{
    uint8_t i = 0U;

    while (true)
    {
        digitalWrite(BUZZER_PIN, HIGH);
        delay(duration);
        digitalWrite(BUZZER_PIN, LOW);

        if (++i == numbeeps)
        {
            return;
        }

        delay(gap);
    }
}

struct Timer
{
    Timer (unsigned t_hours, unsigned t_minutes, unsigned t_seconds, uint8_t t_numbeeps, unsigned long t_beep_dur, unsigned long t_beep_gap) : hours(t_hours), minutes(t_minutes), seconds(t_seconds),
        numbeeps(t_numbeeps), beep_dur(t_beep_dur), beep_gap(t_beep_gap)
    {
        while (seconds > 59)
        {
            minutes += 1;
            seconds -= 60;
        }

        while (minutes > 59)
        {
            hours += 1;
            minutes -= 60;
        }
    }

    unsigned hours = 0;
    unsigned minutes = 0;
    unsigned seconds = 0;
    uint8_t numbeeps = 1;
    unsigned long beep_dur = 300;
    unsigned long beep_gap = 100;
};

// TODO: Can probably simplify function code by making this an array
Timer workTimer{DEFAULT_WORK_HOURS, DEFAULT_WORK_MINUTES, DEFAULT_WORK_SECONDS, DEFAULT_WORK_NUM_BEEPS, DEFAULT_WORK_BEEP_DUR, DEFAULT_WORK_BEEP_GAP};
Timer shortTimer{DEFAULT_SHORT_BREAK_HOURS, DEFAULT_SHORT_BREAK_MINUTES, DEFAULT_SHORT_BREAK_SECONDS, DEFAULT_SHORT_BREAK_NUM_BEEPS, DEFAULT_SHORT_BREAK_BEEP_DUR, DEFAULT_SHORT_BREAK_BEEP_GAP};
Timer longTimer{DEFAULT_LONG_BREAK_HOURS, DEFAULT_LONG_BREAK_MINUTES, DEFAULT_LONG_BREAK_SECONDS, DEFAULT_LONG_BREAK_NUM_BEEPS, DEFAULT_LONG_BREAK_BEEP_DUR, DEFAULT_LONG_BREAK_BEEP_GAP};

unsigned numWorkToLong = DEFAULT_WORK_TO_LONG_BREAK;

struct Button
{
    int pin = 0;
    bool cur_state = false;
    bool last_state = false;
    unsigned long last_debounce = 0;
    unsigned long debounce_delay = 50;

    bool Read()
    {
        return digitalRead(pin) > 0;
    }

    void setPin(int t_pin)
    {
        pin = t_pin;
    }

    void setDelay(unsigned long t_delay)
    {
        debounce_delay = t_delay;
    }
} CtrlBtns[4];

struct Screen
{
    char grid[DEFAULT_LCD_ROWS][DEFAULT_LCD_COLS + 1]{};

    void setGrid(int i, const char* str)
    {
        strncpy(grid[i], str, DEFAULT_LCD_COLS);
    }
} menuScreens[3];

void printScreen(const Screen* screen)
{
    lcd.clear();

    for (int i = 0; i < DEFAULT_LCD_ROWS; ++i)
    {
        lcd.setCursor(0, i);
        lcd.print(screen->grid[i]);
    }
}

void updateScreens(int n)
{
    char buf[DEFAULT_LCD_COLS + 1]{};

    if (n == 0)
    {
        sprintf(buf, "Work timer: %d", workTimer.minutes);
        menuScreens[0].setGrid(2, buf);
    }
    else if (n == 1)
    {
        sprintf(buf, "Short timer: %d", shortTimer.minutes);
        menuScreens[1].setGrid(2, buf);
    }
    else if (n == 2)
    {
        sprintf(buf, "Long timer: %d", longTimer.minutes);
        menuScreens[2].setGrid(2, buf);
    }
}

// TODO: Implement Menu
void openMenu()
{
    Screen* curScreen = menuScreens;
    int screenIndex = 0;
    printScreen(curScreen);

    delay(500);

    while (true)
    {
        if (CtrlBtns[0].Read())
        {
            delay(500);
            return;
        }
        else if (CtrlBtns[1].Read())
        {
            if (screenIndex == 2)
            {
                curScreen = menuScreens;
                screenIndex = 0;
            }
            else
            {
                ++curScreen;
                ++screenIndex;
            }
        }
        else if (CtrlBtns[2].Read())
        {
            if (screenIndex == 0)
            {
                workTimer.minutes -= 1;
            }
            else if (screenIndex == 1)
            {
                shortTimer.minutes -= 1;
            }
            else if (screenIndex == 2)
            {
                longTimer.minutes -= 1;
            }
        }
        else if (CtrlBtns[3].Read())
        {
            if (screenIndex == 0)
            {
                workTimer.minutes += 1;
            }
            else if (screenIndex == 1)
            {
                shortTimer.minutes += 1;
            }
            else if (screenIndex == 2)
            {
                longTimer.minutes += 1;
            }
        }

        updateScreens(screenIndex);
        printScreen(curScreen);
        delay(500);
    }
}

void pomodoro()
{
    Timer active = workTimer;
    Timer next = shortTimer;

    unsigned numWork = 0;
    bool isWork = true;

    while (true)
    {
        if (CtrlBtns[0].Read())
        {
            updateScreens(0);
            openMenu();
            active = workTimer;
            next = shortTimer;
        }
        else if (CtrlBtns[1].Read())
        {
            delay(500);

            while (!CtrlBtns[1].Read())
            {
                delay(100);
            }
        }

        lcd.clear();
        lcd.home();

        lcd.print("Work Cycles: ");
        lcd.print(numWork);

        lcd.setCursor(0, 2);

        lcd.print(isWork ? "Work : " : "Break: ");

        if (active.hours < 10)
        {
            lcd.print(0);
        }

        lcd.print(active.hours);
        lcd.print(':');

        if (active.minutes < 10)
        {
            lcd.print(0);
        }

        lcd.print(active.minutes);
        lcd.print(':');

        if (active.seconds < 10)
        {
            lcd.print(0);
        }

        lcd.print(active.seconds);

        lcd.setCursor(0, 3);
        lcd.print(isWork ? "Break: " : "Work : ");

        if (next.hours < 10)
        {
            lcd.print(0);
        }

        lcd.print(next.hours);
        lcd.print(':');

        if (next.minutes < 10)
        {
            lcd.print(0);
        }

        lcd.print(next.minutes);
        lcd.print(':');

        if (next.seconds < 10)
        {
            lcd.print(0);
        }

        lcd.print(next.seconds);

        if (active.seconds == 0)
        {
            if (active.minutes == 0)
            {
                if (active.hours == 0)
                {
                    if (isWork)
                    {
                        active = next;
                        next = workTimer;
                        ++numWork;
                        isWork = false;
                        beep(active.numbeeps, active.beep_dur, active.beep_gap);
                        delay(1000);
                    }
                    else
                    {
                        active = next;
                        next = (numWork % numWorkToLong == numWorkToLong - 1 ? longTimer : shortTimer);
                        isWork = true;
                        beep(active.numbeeps, active.beep_dur, active.beep_gap);
                        delay(1000);
                    }

                    continue;
                }

                active.hours -= 1;
                active.minutes = 59U;
            }
            else
            {
                active.minutes -= 1;
                active.seconds = 59U;
            }
        }
        else
        {
            active.seconds -= 1;
        }

        delay(1000);
    }
}

void setup()
{
    Serial.begin(9600);
    pinMode(BUZZER_PIN, OUTPUT);
    pinMode(8, INPUT);
    pinMode(9, INPUT);
    pinMode(10, INPUT);
    pinMode(11, INPUT);
    digitalWrite(BUZZER_PIN, LOW);
    lcd.init();
    lcd.backlight();
    lcd.home();

    CtrlBtns[0].setPin(8);
    CtrlBtns[1].setPin(9);
    CtrlBtns[2].setPin(10);
    CtrlBtns[3].setPin(11);

    menuScreens[0].setGrid(0, "Menu");
    menuScreens[1].setGrid(0, "Menu");
    menuScreens[2].setGrid(0, "Menu");

    pomodoro();
}

void loop()
{
}
